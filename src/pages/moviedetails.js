import { useState,useEffect} from "react"
import {Cardholders,Card,H5,P,Cardbutton,MovieContainer}from '../style/servicesStyle'
import GetMovieDetails from "../components/getDetails"


export default (props) =>{
    const [similar,setSimilar] = useState([]);
    const [reco,setReco] = useState([]);
    const [review,setRevies] = useState([]);
    const ApiKey = 'b547daa6db4eb18de0d6fda4fd481159';
    let Movie_id=props.location.state.id;
    const fetchDetails =(id,opt,func)=>{
        fetch(`https://api.themoviedb.org/3/movie/${id}/${opt}?api_key=${ApiKey}&language=en-US&page=1`)
        .then(res => res.json())
        .then(data =>func(data.results) )
    }
   useEffect(()=>{
       fetchDetails(Movie_id,'similar',setSimilar)
       fetchDetails(Movie_id,'recommendations',setReco)
       fetchDetails(Movie_id,'reviews',setRevies)
   },[])

    return(
        <div>
            <h1>review</h1>
            {console.log(review)}
          <MovieContainer >
    <Cardholders>
   {review.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.author}</H5>
       <P>{movie.content}</P>

      
   </Card>

   ))}
   <h1>recommendations</h1>
   </Cardholders>
     </MovieContainer>
          <MovieContainer >
    <Cardholders>
   {reco.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>

      
   </Card>

   ))}
   
   </Cardholders>
     </MovieContainer>
     <h1>Similar</h1>
          <MovieContainer >
    <Cardholders>
   {similar.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>

      
   </Card>

   ))}
   </Cardholders>
     </MovieContainer>

            </div>
    )
}