
import{Subinput,Subbutton,Subtextarea,Registerdiv,Div,AdjustLogin} from '../style/prefooterStyle'
import { useState } from 'react'
export default   () =>{
  const divstyel={
    width:'100%',
    height:'100%',
    background:'black'
  } 
  const [count,setcount] =useState(0)
  const [user,setUser] = useState({
    name:'',
    password:'',
    email:'',
  })

  const handleSubmit = (e)=>{
 
    e.preventDefault();
    setcount(count+1)
    localStorage.setItem(`${user.name}`,JSON.stringify(user))
  window.location.href="http://localhost:3000"
    
  }

    return(
       <Div>
         <AdjustLogin onSubmit={handleSubmit}>
             <Registerdiv>

           <h3 style={{textAlign:"start",color:"white",fontFamily:'pt serif',fontSize:"22px",marginTop:'0',padding:"8px 0"}} >Register Your Email</h3>
           <Subinput type="text" placeholder="Enter Your Name..." name="name" onChange={(e)=>setUser({...user,name:e.target.value})}/>
           <Subinput type="text" placeholder="Enter Your Email..." name="email" onChange={(e)=>setUser({...user,email:e.target.value})}/>
           <Subinput type="password" placeholder="Enter Your Password..."/>
           <Subinput type="password" placeholder="Confirm Your Password..." name="password"onChange={(e)=>setUser({...user,password:e.target.value})}/>
         
         <Subbutton width >Submit</Subbutton>
           
         
          
           </Registerdiv>
           </AdjustLogin>
           </Div> 
    )
}