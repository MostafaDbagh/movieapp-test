
import { faHandHolding } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'
import{Subinput,Subbutton,Subtextarea,Registerdiv,Div,AdjustLogin} from '../style/prefooterStyle'

export default   () =>{
const [checkuser,setCheckuser] =useState({
  name:'',
  password:'',
})

  const divstyel={
    width:'100%',
    height:'100%',
    background:'black'
  }
  const handleSubmit = (e)=>{
  e.preventDefault();
  let data = JSON.parse( localStorage.getItem(`${checkuser.name}`))
 if(data )
{
  let {name,password,email}=data;
  if(name === checkuser.name && password===checkuser.password){
    window.location.href = "http://localhost:3000/Home";
  }
 }else{
  window.location.href = "http://localhost:3000/Register";
}}
    return(
       <Div>
         <AdjustLogin onSubmit={handleSubmit}>
             <Registerdiv>

           <h3 style={{textAlign:"start",color:"white",fontFamily:'pt serif',fontSize:"22px",marginTop:'0',padding:"8px 0"}} >Login Your Email</h3>
           <Subinput type="text" placeholder="Enter Your Name..." onChange={(e)=>setCheckuser({...checkuser,name:e.target.value})}/>
           <Subinput type="password" placeholder="Enter Your Password..."onChange={(e)=>setCheckuser({...checkuser,password:e.target.value})}/>
         <div style={{display:"flex"}}>
         <Subbutton hasyellow onClick={()=>window.location.href="http://localhost:3000/Register"}>Register</Subbutton>
           <Subbutton>Login</Subbutton>
         </div>
          
           </Registerdiv>
           </AdjustLogin>
           </Div> 
    )
}