
import Home from './Home'
import Login from './Login'
import Nowapplayingpage from './Nowapplayingpage'
import moviedetails from './moviedetails'
import Tvdetails from './Tvdetails'
import Register from './Register'


export {
    Home,
    Login,
    Register,
    Tvdetails,
    moviedetails,
    Nowapplayingpage
}