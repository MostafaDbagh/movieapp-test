import {Switch,
  Route,
  BrowserRouter as Router
} from 'react-router-dom'
import {
  Home,
  Register,
  Login,
  Tvdetails,
  Nowapplayingpage,
  moviedetails
}from './pages'


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
        
         <Route exact path='/' component={Login}  />
         <Route  path='/Register'component={Register}   />
          <Route  path='/Home' component={Home}   />
          <Route  path='/Nowapplaying' component={Nowapplayingpage}  />
            <Route  path='/Movie-Details'component={moviedetails}  />
          <Route  path='/Tv-Details'component={Tvdetails}   />
       
          
          
         
        </Switch> 
        </Router>
    </div>
  );
}

export default App;
