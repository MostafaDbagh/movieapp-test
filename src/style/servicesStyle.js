import styled from "styled-components";

export const MovieContainer = styled.div`
width:98%;


margin:32px auto;
position:relative;


`
export const H2 = styled.h2`
margin:32px auto;
text-align:center
`
export const Cardholders = styled.section`
width:96%;

margin:18px auto 72px auto;
display:flex;
justify-content:space-between;
flex-wrap:wrap
`
export const Card = styled.div`

width:28%;
height:400px;
background:rgb(16,30,40);
margin-top:14px;
border-top-right-radius:4px;



`
export const H5 = styled.h5`
color:white;
padding:16px 0;
text-align:center;
position:relative;
font-size:19px;
font-family: varela round, sans-serif;


`
export const P = styled.p`
color:white;
padding:0 24px ;
font-family:pt serif,sans-serif;
font-size:15px;
`
export const Cardbutton =styled.button`
margin: 14px 0 0 27%;
text-align:center;
height:38px;
padding:8px 14px;
width:140px;
background:#e3e3e3;
border:none;
border-radius:2px;
background:rgb(255,196,12);
font-family:pt serif,sans-serif;
font-weight:700;
color:black;
`