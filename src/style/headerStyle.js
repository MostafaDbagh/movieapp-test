import styled from "styled-components";



export const Div = styled.div`
box-sizing:border-box;
width:100%;
justify-content:center;
display:flex;
min-height:120px;
background:white;
flex-direction:column;
border-bottom:5px solid rgb(0,15,25);
`
export const Logo = styled.section`
margin:0 auto;
height:80px;

`
export const HeaderSec = styled.section`
display:flex;

`
export const Button = styled.button`
width:110px;
height:36px;
padding:6px 18px;
background:rgb(255,196,12);
margin-left:12px;
font-size:14px;
font-weight:600;
border-radius:5px;
font-family:pt serif;
color:rgb(16,30,40);
border:none;

`
export const UnorderHeader = styled.ul`
flex-basis:90%;
margin:0 0 0 26px;
padding:0;
display:flex;
height:50px;
justify-content:space-around;
align-items:center;

`

export const ListHeader = styled.li`
list-style:none;
padding:4px 8px;
margin:4px;
`
export const Buttondiv = styled.div`
flex-basis:35%;
display:flex;
justify-content:flex-start;
align-items:center;
flex-direction:row-reverse;

`
export const Anchor = styled.li`
text-decoration:none;
font-size:16px;
font-weight:bold;
color:rgb(0,15,25);
font-family:varela round ,sans-serif;
&:hover{
    color:rgb(255,196,16)
}
`
export const Link = styled.link`
text-decoration:none;
font-size:16px;
font-weight:bold;
color:rgb(0,15,25);
font-family:varela round ,sans-serif;
&:hover{
    color:rgb(255,196,16)
}
`