import styled  from 'styled-components'

import one from '../images/1.jpg'


export const Container = styled.div`
background:url(${one});
width:100%;
height:520px;
background-size:cover;
display:flex;
flex-direction:column;
position:relative;
`
export const Overlay = styled.div`
width:100%;
height:100%;
background:rgba(0,0,0,0.7);
`
export const H1 = styled.h1`
color:white;
text-align:start;
padding:100px 0 0;
font-family:pt serif;
margin-left:12px;
letter-spacing:1px;
text-align:center;
`
export const Paragraph = styled.p`
color:white;
width:60%;
margin:8px auto 32px; 
font-family:varela round;
font-size:18px;
line-height:25px;
text-align:center;


`
export const Input= styled.input`
width:440px;
height:40px;
padding: 10px ;
margin:0 4px 0 2rem;
border-radius:5px;
font-family:varela round ,sans-serif;
outline:0;
border:0;


`
export const Button = styled.button`
width:160px;
height:60px;
border:2px solid rgb(255,196,12);
padding:10px;
outline:none;
background:none;
color:white;
font-family:varela round ,sans-serif;
font-weight:900;
border-radius:5px;

font-size:16px;
box-shadow: 0 0 2px 1px rgb(0,15,25);


`