import styled from 'styled-components'


export const Div  = styled.div`
width:100vw;
min-height:100vh;
background:black;
position:relative;
`
export const AdjustLogin= styled.form`
width:40%;
min-height:260px;
background:rgb(0,15,25);
position:absolute;
top:50%;
left:50%;
transform:translate(-50%,-50%)
`
export const Registerdiv= styled.div`

display:flex;
flex-direction:column;

justify-content:center;
align-items:center;
`    
export const Subtextarea  = styled.textarea`
width:270px;
height:70px;
padding:6px 12px;
border-radius:2px;
margin-bottom:20px;
outline:none;
box-shadow:0 0 2px 0px rgb(255,196,12);

`
export const Subinput = styled.input`
width:270px;
height:27px;
border-radius:2px;
border:none;
margin-bottom:20px;
padding:6px 12px;
outline:none;
box-shadow:0 0 2px 0px rgb(255,196,12);
&::placeholder{
    font-family:pt serif,sans-serif;
    
}

`
export const Subbutton=styled.button`
margin: 0px 0 10px 8px;
height:38px;
padding:8px 14px;
width:${props => props.width ? '57%':"140px"};

background:${props=>props.hasyellow ? 'rgb(255,196,12)':'#5bc0de'};
border:none;
border-radius:2px;

font-family:pt serif,sans-serif;
font-weight:bold;
color:black;
font-style:italic;
font-size:14px;






`
