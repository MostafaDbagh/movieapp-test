import React,{useState,useEffect} from "react";

import {faQuoteLeft,faLongArrowAltLeft,faLongArrowAltRight} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default function SimpleSlider() {
 const review =[
        { "name":"Rahul Chandy","review":"We bought the Yamazaki Japanese Cheesecake and the Brownie like cupcake. It was not very expensive, but tasted really great. The cheesecake was not very sweet, and had a delicious cream filling."},
        {"name":"Tony Day","review":"Excellent bread products for snacks,dessert or novelty ( picachu,puppy,panda and others).Selection of infusion drinks and cold drinks.  Very well presented and just feels so clean. Nice Japanese styling.  Family loved the breadWell worth a try."},
        {"name":"Dee Emasa","review":"Good quality of Japanese food and perfect place to go in al ain , they have a very tasty sweets and savories 🥖🍪🧁🍗🥐😍."},
        {"name":"Alya Al Zaabi","review":"A tasty Japanese bakery, I like the savory pastries more than the sweets. Go for the jalapeño hotdog 🌭 and chicken curry bun .. it’s really tasty and prices are good and they have Fazaa card promotion."},
        {"name":"Edward Cahayagan","review":"If you wang to satisfy your apetite on savory and sweet come at yamazaki bakery were you can eat and taste the work of art of japanese bread...☺️☺️☺️"}
    ]
    let [i,setI] = useState(0)

setTimeout(() => {
    if(i <=3) setI(++i)
    else{
       
        setI(0)
        
    }
}, 4000);
const increment = ()=>(
    i <= 3 ? setI(++i) :setI(0)
)
const decrement = ()=>(
    i >=3  ? setI(--i) :setI(4)
)
  return (
      <div style={{marginTop:"32px",background:'rgb(0,15,25)',position:'relative'}}>
   <div style={{width:'100%',margin:'24px auto 0 auto',display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
 <div >
 <h2 style={{textAlign:"center",color:"white",fontFamily:'pt serif'}} >Customers Say About Us</h2>
 </div>
  
   <div style={{margin:"32px auto 0 auto",width:"90%",borderRadius:"10px",color:"black"}}>
    <div style={{textAlign:"center",margin:"10px 0"}}>
        <FontAwesomeIcon icon={faQuoteLeft} style={{fontSize:"32px",marginBottom:"4px",color:"rgb(255,196,12)"}}/>
        </div>
    
    </div>

   </div>
   <div style={{width:"90%",margin:'0 auto',height:'240px',overflow:'hidden',display:'flex',flexDirection:'column-reverse'}}>

<div style={{display:'flex',justifyContent:'space-between',marginBottom:'20px;'}}>
  <button onClick={()=>decrement()} style={{background:'none',border:'none',outline:'none',fontSize:'28px',color:'rgb(255,196,12)'}}><FontAwesomeIcon icon={faLongArrowAltLeft}></FontAwesomeIcon></button>
  <button  onClick={()=>increment()} style={{background:'none',border:'none',outline:'none',fontSize:'28px',color:'rgb(255,196,12)'}}><FontAwesomeIcon icon={faLongArrowAltRight}></FontAwesomeIcon></button>
  </div>
 
  <div style={{height:"210px"}}>
      <p style={{width:'50%',margin:'2px auto 0 auto',padding:'12px 12px',minHeight:'80px',color:'rgb(255,255,255)'
      ,fontFamily:'varela round ,sans-serif'}}>{review[i].review}</p>
      <h3 style={{width:'60%',margin:'0 auto',textAlign:'center',color:'rgb(255,196,12)',fontFamily:'pt serif'}}>{review[i].name}</h3>
  </div>


           
       </div>

   </div>
    
  );
}