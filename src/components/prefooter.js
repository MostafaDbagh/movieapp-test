import{Div,ColumnSec,ArticleSec,Subinput,Subbutton,Subtextarea,Registerdiv} from '../style/prefooterStyle'
import { Cardbutton } from '../style/servicesStyle';
import React from 'react'
import Aos from 'aos';
import{useState,useEffect,useRef} from 'react'
import {Anchor,ListHeader} from '../style/headerStyle'
import { FaEnvelope,FaFacebook,FaMobile } from 'react-icons/fa';
import "aos/dist/aos.css";



import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'



const Subscribe = () => {
   const spanRef1 =useRef();
   const inputRef1 =useRef();

    const [sub_email,setSubemail] = useState('')
    useEffect(()=>{
        Aos.init({duration:2000})
        Aos.refresh()
    },[])
    const emailCheck = (email)=>{
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const handleSubscribe = async()=>{
        if(!emailCheck(sub_email)) {
            spanRef1.current.style.display='block'
            return
       
        }
   

    }
    return ( 
       <Div>
           
           {/* <Registerdiv>
           <h3 style={{textAlign:"start",color:"white",fontFamily:'pt serif',fontSize:"22px",marginTop:'0'}} >Register Your Email</h3>
           <Subinput type="text" placeholder="Enter Your Name..."></Subinput>
           <Subinput type="text" placeholder="Enter Your Email..."></Subinput>
          <Subtextarea placeholder="Enter your Message..."></Subtextarea>
           <Subbutton>Register</Subbutton>
           </Registerdiv> */}

           <section style={{marginTop:'32px'}} >
               <h3>
               Information
               </h3>
               <ul style={{margin:'0',padding:'0'}}  ><ListHeader><Anchor href="#" style={{fontSize:'14px'}}>About Us</Anchor></ListHeader>
            <ListHeader><Anchor href="#" style={{fontSize:'16px'}}>News</Anchor></ListHeader>
              <ListHeader><Anchor href="/Login" style={{fontSize:'16px'}}>Career</Anchor></ListHeader>
              <ListHeader><Anchor href="#" style={{fontSize:'16px'}}>Works Example</Anchor></ListHeader></ul>
           </section>
           <section style={{marginTop:'32px'}}>
              <h3>Social Media</h3> <ul style={{margin:'0',padding:'0'}}>
              <ListHeader><Anchor href="#" style={{fontSize:'16px',}}><FaEnvelope style={{marginRight:'6px',color:'rgb(255,196,19)',fontSize:'18px'}} /> MostafaDbagh@gmail.com</Anchor></ListHeader>
              <ListHeader><Anchor href="#" style={{fontSize:'16px'}}><FaFacebook style={{marginRight:'6px',color:'rgb(255,196,19'}} />Facebook</Anchor></ListHeader>
              <ListHeader><Anchor href="#" style={{fontSize:'16px'}}><FaMobile style={{marginRight:'6px',color:'rgb(255,196,19'}}/>0586057772</Anchor></ListHeader></ul>
           </section>
       </Div>
       
     );
}
 
export default Subscribe;
