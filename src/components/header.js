import React from 'react';
import { Link } from 'react-router-dom';

import { Div,HeaderSec,Logo,UnorderHeader,ListHeader,Button,Buttondiv} from '../style/headerStyle';

const Header = () => {
    const AnchorStyle= {
        textDecoration:'none',
        fontSize:'16px',
        fontWeight:'bold',
        fontFamily:"varela round ,sans-serif",
        color:"rgb(0,15,25)",

    }
    return ( 

        <Div>
          
            <Logo>
       <h2>Movie App</h2>
    </Logo>
     <HeaderSec>
<UnorderHeader>
    <ListHeader><Link to="/Home" style={AnchorStyle} >Home</Link></ListHeader>
    <ListHeader><Link to="/Movie-Details"style={AnchorStyle} >Movie-Details</Link></ListHeader>
    <ListHeader><Link to="/Tv-Details"style={AnchorStyle}>Tv-Details</Link></ListHeader>
    <ListHeader><Link to="/Nowapplaying"style={AnchorStyle}>Nowapplaying</Link></ListHeader>
    <ListHeader><Link to="/Register"style={AnchorStyle}>Register</Link></ListHeader>
    <ListHeader><Link to="/"style={AnchorStyle}>Login</Link></ListHeader>
</UnorderHeader>


     </HeaderSec>

        </Div>
            
      
    );
}
 
export default Header;