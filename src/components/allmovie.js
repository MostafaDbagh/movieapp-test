
import {Cardholders,Card,H5,P,Cardbutton,MovieContainer}from '../style/servicesStyle'
import { useState,useEffect } from 'react'
import {Link} from 'react-router-dom'

import GetMovieDetails from './getDetails';
export const Allmovie= ()=>{
    const ApiKey = 'b547daa6db4eb18de0d6fda4fd481159';

    let [upcoming,setUpcoming] = useState([]);
    let [latestmovie,setLatestmovie] = useState([]);
    let [toprated,setTopRated] = useState([]);
    let [tvtoprated,setTvtoprated] = useState([]);
    let [tvpopular,setTvpupular] = useState([]);
    let [tvlastest,setTvlastest] = useState([]);
   
     const sendData=(e)=>{
        
     }
    const fetchFunc = (func,opt)=>{
        fetch(` https://api.themoviedb.org/3/movie/${opt}?api_key=${ApiKey}`)
        .then(res => res.json())
        .then(data => func(data.results))
        .catch(e =>alert(e))
    }
    const fetchFunc2 = (func,opt)=>{
        fetch(`https://api.themoviedb.org/3/tv/${opt}?api_key=${ApiKey}`)
        .then(res => res.json())
        .then(data => func(data.results))
        .catch(e =>alert(e))
    }
    useEffect(()=>{
        Promise.all([
        fetchFunc(setLatestmovie,'popular'),
        fetchFunc(setUpcoming,'upcoming'),
        fetchFunc(setTopRated,'top_rated'),
        fetchFunc2(setTvtoprated,'top_rated'),
        fetchFunc2(setTvpupular,'popular'),
        fetchFunc2(setTvlastest,'on_the_air')
        ])
       
    },[setUpcoming,setTopRated,setLatestmovie])
     
    return(
        
        <>
      
        <h1>Latest</h1>
     
        <MovieContainer >
    <Cardholders>
   {latestmovie.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
      
   </Card>

   ))}
   </Cardholders>
     </MovieContainer>

    <h2>Top Rated Movie</h2>
    <MovieContainer text="All Movie">
    <Cardholders>
   {toprated.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
   </Card>

   ))}
   </Cardholders>
     </MovieContainer>


  
    <h2>Upcoming Movie</h2>
    <MovieContainer text="All Movie">
    <Cardholders>
   {upcoming.map(movie =>(
      
  
   <Card  key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
   </Card>

   
     
   ))}
   </Cardholders>
     </MovieContainer>

        <h1> TV Latest</h1>
       
         <MovieContainer >
    <Cardholders> 
    {tvlastest.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
   </Card>

   ))}
   </Cardholders>
     </MovieContainer> 

    <h2>TV Top Rated </h2>
    <MovieContainer text="All Movie">
    <Cardholders>
   {tvtoprated.map(movie =>(
   <Card key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
   </Card>

   ))}
   </Cardholders>
     </MovieContainer> 

    <h2>Tv Popular </h2>
    <MovieContainer text="All Movie">
    <Cardholders>
   {tvpopular.map(movie =>(
   <Card  key={movie.id}>
       <H5>{movie.title}</H5>
       <P>{movie.overview}</P>
       <Link to={{ pathname: `/movie-details`,state:{id:movie.id} }}  >More Details</Link>
   </Card> 
   ))}
   </Cardholders>
     </MovieContainer>


    </>
    )
}